$.fn.scrollEnd = function(callback, timeout) {
    $(this).scroll(function(){
        var $this = $(this);
        if ($this.data('scrollTimeout')) {
            clearTimeout($this.data('scrollTimeout'));
        }
        $this.data('scrollTimeout', setTimeout(callback,timeout));
    });
};
//TODO: return 1*1 pixels
$( document ).ready(function() {

    console.log('client loaded');
    window.addEventListener('message', function (event) {
        if (event.origin !== 'http://localhost:9000') {
            return;
        }
        else {
            spy.onMessage(event);
        }
    }, false);

    // check if it is not an iframe
    if(window.self === window.top) {

        sessionStorage.sessionClosed = true;

        //generate uuid
        spy.createNewSession();

        // register scroll
        $(window).scrollEnd(function () {
            spy.recordScrollPosition();
        }, 500);

        // add onLinkClick(pageLink) to all link in the page
        $("a").each(function () {
            $(this).on("click", function () {
                spy.onLinkClick(this.href);
                sessionStorage.sessionClosed = false;
            });
        });

        // add onInputChange(id, type, value) to all inputs in the page on click or focusout event
        $("input").each(function () {
            if (this.type === 'button' || this.type === 'submit' || this.type === 'radio'
                || this.type === 'checkbox' || this.type === 'reset' || this.type === 'number'
                || this.type === 'range') {
                $(this).on("click", function () {
                    spy.onInputChanged(this.id, this.type, this.value);
                });

            } else {
                $(this).on("focusout", function () {
                    spy.onInputChanged(this.id, this.type, this.value);
                });
            }
        });

        // add onInputChange(id, type, value) for all selects in page on change event
        $("select").each(function () {
            $(this).on("change", function () {
                spy.onInputChanged(this.id, this.type, this.value);
            });
        });

        //TODO: check on this one
        //add getSelected text action
        $(document).bind("mouseup", spy.getSelected);

        $(window).on('beforeunload', function() {
            if(sessionStorage.sessionClosed === "true") {
                spy.requestIMG.src = "http://localhost:8080/spybl-web/session/close?uuid="+sessionStorage.uuid;
            }
        });
    }
});

var utils = {

    allShades : [],

    Interpolate : function(start, end, steps, count) {
        var s = start,
            e = end,
            final = s + (((e - s) / steps) * count);
        return Math.floor(final);
    },

    Color : function(_r, _g, _b) {
    var r, g, b;
        var setColors = function(_r, _g, _b) {
            r = _r;
            g = _g;
            b = _b;
        };

        setColors(_r, _g, _b);
        this.getColors = function() {
            return colors = {
                r: r,
                g: g,
                b: b
            };
        };
    },

    getShade : function(steps, index, occ) {

        var start = new utils.Color(255, 200, 200),
            end = new utils.Color(255, 0, 0);

        var startColors = start.getColors(),
            endColors = end.getColors();
        var r = utils.Interpolate(startColors.r, endColors.r, steps, index);
        var g = utils.Interpolate(startColors.g, endColors.g, steps, index);
        var b = utils.Interpolate(startColors.b, endColors.b, steps, index);

        var shade = "style = 'background-color : rgb(" + r + "," + g + "," + b + ")' !important;";

        if(typeof utils.allShades[occ] !== 'undefined') {
            return utils.allShades[occ];

        } else {
            utils.allShades[occ] = shade;
            return shade;
        }


    }
};

var spy = {

    requestIMG : document.createElement("IMG"),


    onMessage : function(messageEvent) {
        if(messageEvent.data["action"] === 'scroll') {
            var scrollTop = messageEvent.data["xPos"];
            var scrollLeft = messageEvent.data["yPos"];
            $(window).scrollTop($(document).height() / scrollTop);
            $(window).scrollLeft($(document).width() / scrollLeft);
        } else if (messageEvent.data["action"] === 'link') {
            document.location.href = messageEvent.data["url"];
        } else if (messageEvent.data["action"] === 'input') {
            var jqueryId = '#' + messageEvent.data["id"];
            var type = messageEvent.data["type"];
            var value = messageEvent.data["value"];
            var elem = $(jqueryId);
            if (type === 'button' || type === 'submit' || type === 'radio'
                || type === 'checkbox' || type ==='reset') {
                elem.click();
            } else {
                elem.val(value);
                if(type === 'select-one') {
                    elem.select2('val',value);
                }
            }
        } else if(messageEvent.data["action"] === 'selectText') {
            var jqueryId = '#' + messageEvent.data["containerID"];
            var text = messageEvent.data["text"];
            $(jqueryId).highlight(text, true);

        } else if(messageEvent.data["action"] === 'report') {
            var steps = messageEvent.data["length"] + 1;
            $(messageEvent.data).each(function( index ) {
                var occ = messageEvent.data[index].numberOfOccurrences;
                var shade = utils.getShade(steps, index, occ);
                var jqueryId = "#" + messageEvent.data[index].elementId;
                var spanValue = "<span class='badge'"+ shade+">" + occ + "</span>";
                $( spanValue ).insertBefore(jqueryId);
            });

        }
    },

    createNewSession : function () {
        var ip = sessionStorage.ip;
        var location = sessionStorage.location;
        var uuid = sessionStorage.uuid;
        if(ip == null || location == null || uuid == null) {
            uuid = spy.guid();
            jQuery.ajax({
                type: "GET",
                url: "http://www.telize.com/geoip?callback=?",
                dataType: "jsonp",
                success: function(location){
                    sessionStorage.ip = location.ip;
                    sessionStorage.location = location.country;
                    sessionStorage.uuid = uuid;

                    spy.requestIMG.src = "http://localhost:8080/spybl-web/session/store?uuid="+sessionStorage.uuid+
                        "&url="+document.URL+"&title="+document.title+
                        "&ip="+sessionStorage.ip+"&location="+sessionStorage.location;
                },
                error: function(){
                    alert("The requested page is not available for the moment. Please try again later !");
                }
            });
        }

    },

    recordScrollPosition : function() {
        var scrollLeft = $(document).width() / $(document).scrollLeft();
        var scrollTop = $(document).height() / $(document).scrollTop();

        if (scrollLeft == 'Infinity') {
            scrollLeft = 0;
        }

        if (scrollTop == 'Infinity') {
            scrollTop = 0;
        }
        spy.requestIMG.src = "http://localhost:8080/spybl-web/actions/position/" + sessionStorage.uuid + "?url="+document.URL+"&title="+document.title+
            "&topPos="+scrollTop.toFixed(2)+"&leftPos=" +scrollLeft.toFixed(2);
    },

    getSelected : function(){
        var text = '';
        var id = "";
        if(window.getSelection){
            text = window.getSelection().toString();
            if(typeof  window.getSelection().anchorNode !== 'undefined' && window.getSelection().anchorNode != null) {
                if (typeof window.getSelection().anchorNode.parentNode.attributes[0] !== 'undefined') {
                    id = window.getSelection().anchorNode.parentNode.attributes[0].nodeValue;
                }
            }

        }else if(document.getSelection){
            text = document.getSelection().toString();
            if(typeof document.getSelection().anchorNode.parentNode.attributes[0].nodeValue !== 'undefined') {
                id = document.getSelection().anchorNode.parentNode.attributes[0].nodeValue;
            }

        }else if(document.selection){
            text = document.selection.createRange().text;
            //id = window.getSelection().anchorNode.parentNode.attributes[0].nodeValue;
        }
        if(text != '' && id != "") {
            spy.requestIMG.src = "http://localhost:8080/spybl-web/actions/selectText/" + sessionStorage.uuid + "?url="+document.URL+"&title="+document.title+
                "&selectedText="+text+"&containerId="+id;
        }
    },

    onLinkClick : function(pageLink) {
        spy.requestIMG.src = "http://localhost:8080/spybl-web/actions/link/" + sessionStorage.uuid + "?url="+document.URL+"&title="+document.title+
            "&link="+pageLink ;
    },

    onInputChanged : function(id, type, value){
        spy.requestIMG.src = "http://localhost:8080/spybl-web/actions/input/" + sessionStorage.uuid + "?url="+document.URL+"&title="+document.title+
            "&type="+type+"&id="+id+"&value="+value ;
    },

    guid : function() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    }
};
