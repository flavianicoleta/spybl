package com.thesis.spybl.web.controllers;

import com.thesis.spybl.model.*;
import com.thesis.spybl.services.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Created by flavia.gheorghe on 26/02/15.
 */
@Controller
public class SessionController {

    @Autowired
    private SessionService sessionService;


    @RequestMapping(value = "/session/store", produces = "application/json", method = RequestMethod.GET)
    public @ResponseBody Session createNewSession(@RequestParam String uuid,
                                               @RequestParam String url, @RequestParam String title,
                                               @RequestParam String ip, @RequestParam String location) {
        Page page = new Page();
        page.setUrl(url);
        page.setTitle(title);
        return sessionService.store(uuid, page, ip, location, System.currentTimeMillis());
    }

    @RequestMapping(value = "/session/close", produces = "application/json", method = RequestMethod.GET)
    public @ResponseBody Long closeSession(@RequestParam String uuid){
        return sessionService.update(uuid, System.currentTimeMillis());
    }

}
