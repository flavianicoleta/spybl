package com.thesis.spybl.web.controllers;

import com.thesis.spybl.model.*;
import com.thesis.spybl.services.ActionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Created by flavia.gheorghe on 08/07/15.
 */
@Controller
public class ActionController {

    //TODO: extract in  properties file action types
    @Autowired
    private ActionService actionService;

    @RequestMapping(value = "/actions/position/{uuid}", produces="application/json", method = RequestMethod.GET)
    public @ResponseBody
    Action getPosition(@PathVariable String uuid,
                     @RequestParam String url, @RequestParam String title,
                     @RequestParam Double topPos, @RequestParam Double leftPos) {

        ScrollAction scrollAction = new ScrollAction();
        scrollAction.setxPosition(topPos);
        scrollAction.setyPosition(leftPos);

        Page page = new Page();
        page.setUrl(url);
        page.setTitle(title);
        return actionService.store("scroll", scrollAction, System.currentTimeMillis(), page, uuid);
    }

    @RequestMapping(value = "/actions/input/{uuid}", produces="application/json", method = RequestMethod.GET)
    public @ResponseBody Action getInput(@PathVariable String uuid,
                                       @RequestParam String url, @RequestParam String title,
                                       @RequestParam String type, @RequestParam String id, @RequestParam String value)  {

        InputAction inputAction = new InputAction();
        inputAction.setType(type);
        inputAction.setId(id);
        inputAction.setValue(value);

        Page page = new Page();
        page.setUrl(url);
        page.setTitle(title);
        return actionService.store("input", inputAction, System.currentTimeMillis(), page, uuid);
    }

    @RequestMapping(value = "/actions/link/{uuid}", produces="application/json", method = RequestMethod.GET)
    public @ResponseBody Action getLink(@PathVariable String uuid,
                                      @RequestParam String url, @RequestParam String title,
                                      @RequestParam String link)  {

        LinkAction linkAction = new LinkAction();
        linkAction.setUrl(link);

        Page page = new Page();
        page.setUrl(url);
        page.setTitle(title);
        return actionService.store("link", linkAction, System.currentTimeMillis(), page, uuid);
    }

    @RequestMapping(value = "/actions/selectText/{uuid}", produces="application/json", method = RequestMethod.GET)
    public @ResponseBody Action getSelectedText(@PathVariable String uuid,
                                              @RequestParam String url, @RequestParam String title,
                                              @RequestParam String selectedText, @RequestParam String containerId)  {

        SelectTextAction selectTextAction = new SelectTextAction();
        selectTextAction.setSelectedText(selectedText);
        selectTextAction.setContainerID(containerId);

        Page page = new Page();
        page.setUrl(url);
        page.setTitle(title);
        return actionService.store("selectText", selectTextAction, System.currentTimeMillis(), page, uuid);
    }
}
