DROP DATABASE monitor;

CREATE DATABASE monitor
  WITH OWNER = postgres
  ENCODING = 'UTF8'
  TABLESPACE = pg_default
  LC_COLLATE = 'English_United States.1252'
  LC_CTYPE = 'English_United States.1252'
  CONNECTION LIMIT = -1;

DROP TABLE IF EXISTS actions;
DROP TABLE IF EXISTS sessions;

CREATE TABLE sessions
(
  id bigserial NOT NULL,
  uuid character varying(100) NOT NULL,
  page JSON,
  ip character varying(20),
  location character varying(300),
  start_date bigint ,
  end_date bigint ,

  CONSTRAINT pk_session PRIMARY KEY (id)
)
WITH (OIDS = FALSE);

-- Indexes for sessions table
CREATE INDEX pki_sessions ON sessions USING btree (id);



CREATE TABLE actions
(
  id BIGSERIAL NOT NULL,
  action_type CHARACTER VARYING(100) NOT NULL,
  details JSON,
  page JSON,
  registered_at BIGINT,
  session_id BIGSERIAL REFERENCES sessions(id),

  CONSTRAINT pk_action PRIMARY KEY (id)
)
WITH (OIDS = FALSE);

  -- Indexes for actions table
CREATE INDEX pki_actions ON actions USING  BTREE (id);


insert into sessions(uuid, page, ip, location, start_date)
values ('33ad5c33-f7c8-adbe-b760-eafad67732cb', 'http://localhost:9200/player-web/index.html', '89.121.222.106','Romania', 45456156);

insert into actions(action_type, details, registered_at, session_id)
values ('scroll', '{ "name": "Book the First", "author": { "first_name": "Bob", "last_name": "White" } }', 4864444,
        (select id from sessions where uuid = '33ad5c33-f7c8-adbe-b760-eafad67732cb'));