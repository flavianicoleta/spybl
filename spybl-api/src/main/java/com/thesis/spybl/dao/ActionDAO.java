package com.thesis.spybl.dao;

import com.thesis.spybl.model.Action;
import com.thesis.spybl.model.Page;

/**
 * Created by flavia.gheorghe on 08/07/15.
 */
public interface ActionDAO {
    Action store (String actionType, Object details, long registration_date, Page page, String sessionUUID);
}
