package com.thesis.spybl.model;

/**
 * Created by flavia.gheorghe on 06/05/15.
 */
public class SelectTextAction {

    private String selectedText;
    private String containerID;

    public String getSelectedText() {
        return selectedText;
    }

    public void setSelectedText(String selectedText) {
        this.selectedText = selectedText;
    }

    public String getContainerID() {
        return containerID;
    }

    public void setContainerID(String containerID) {
        this.containerID = containerID;
    }
}
