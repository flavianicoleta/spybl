package com.thesis.spybl.model;

/**
 * Created by flavia.gheorghe on 02/05/15.
 */
public class LinkAction {

    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
