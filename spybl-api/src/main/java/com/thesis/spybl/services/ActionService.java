package com.thesis.spybl.services;

import com.thesis.spybl.model.Action;
import com.thesis.spybl.model.Page;

/**
 * Created by flavia.gheorghe on 08/07/15.
 */
public interface ActionService {
    Action store (String actionType, Object details, long registeredAt, Page page, String sessionUUID);
}
