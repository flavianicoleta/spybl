package spybl;

/**
 * Created by flavia.gheorghe on 08/07/15.
 */
public class Action {
    private long id;
    private String actionType;
    private Object details;
    private Page page;
    private long registrationDate;
    private long sessionId;

    public Action(){}

    public Action(long id, String actionType, Object details, Page page, long registrationDate, long sessionId) {
        this.id = id;
        this.actionType = actionType;
        this.details = details;
        this.page = page;
        this.registrationDate = registrationDate;
        this.sessionId = sessionId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public Object getDetails() {
        return details;
    }

    public void setDetails(Object details) {
        this.details = details;
    }

    public Page getPage() {
        return page;
    }

    public void setPage(Page url) {
        this.page = url;
    }

    public long getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(long registrationDate) {
        this.registrationDate = registrationDate;
    }

    public long getSessionId() {
        return sessionId;
    }

    public void setSessionId(long sessionId) {
        this.sessionId = sessionId;
    }
}
