package spybl;

/**
 * Created by flavia.gheorghe on 26/02/15.
 */
public class Session {

    private long id;

    private String uuid;

    private Object page;

    private String ip;

    private String location;

    private long startDate;

    private long endDate;

    public Session(){}

    public Session(long id, String uuid, Object page, String ip, String location, long startDate, long endDate) {
        this.id = id;
        this.uuid = uuid;
        this.page = page;
        this.ip = ip;
        this.location = location;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Object getPage() {
        return page;
    }

    public void setPage(Object page) {
        this.page = page;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public long getStartDate() {
        return startDate;
    }

    public void setStartDate(long startDate) {
        this.startDate = startDate;
    }

    public long getEndDate() {
        return endDate;
    }

    public void setEndDate(long endDate) {
        this.endDate = endDate;
    }
}
