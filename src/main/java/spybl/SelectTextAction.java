package spybl;

/**
 * Created by flavia.gheorghe on 06/05/15.
 */
public class SelectTextAction {

    private String selectedText;

    public String getSelectedText() {
        return selectedText;
    }

    public void setSelectedText(String selectedText) {
        this.selectedText = selectedText;
    }
}
