package spybl;

import com.thesis.spybl.model.Session;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.io.IOException;

/**
 * Created by flavia.gheorghe on 26/02/15.
 */
@Repository
@SuppressWarnings("unchecked")
public class SessionDAOImpl implements SessionDAO {

    @Autowired
    @Qualifier(value = "namedJdbcTemplate")
    private NamedParameterJdbcTemplate namedJdbcTemplate;

    @Override
    public Session store(String uuid, Object page, String ip, String location, long startDate) {

        int response = 0;
        String sql = "INSERT INTO sessions(uuid, page, ip, location, start_date, end_date) " +
                "values (:uuid, json(:page), :ip, :location, :start_date, :end_date)";

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String pageJson = objectMapper.writeValueAsString(page);
            MapSqlParameterSource params = new MapSqlParameterSource();

            params.addValue("uuid", uuid);
            params.addValue("page", pageJson);
            params.addValue("ip", ip);
            params.addValue("location", location);
            params.addValue("start_date", startDate);
            params.addValue("end_date", 0);

            KeyHolder holder = new GeneratedKeyHolder();

            response = namedJdbcTemplate.update(sql, params, holder);

            if(response == 1 && holder.getKeys() != null) {
                long id = (Long)holder.getKeys().get("id");
                Session session = new Session(id, uuid, page, ip, location, startDate, 0);
                return session;

            }
        } catch (IOException e) {
            return  null;
        }


        return null;
    }

    @Override
    public Long update(String uuid, long endDate) {
        int response = 0;
        String sql = "UPDATE sessions set end_date = :end_date where uuid = :uuid";

        MapSqlParameterSource params = new MapSqlParameterSource();

        params.addValue("uuid", uuid);
        params.addValue("end_date", endDate);

        KeyHolder holder = new GeneratedKeyHolder();

        response = namedJdbcTemplate.update(sql, params, holder);

        if(response == 1 && holder.getKeys() != null) {
            return (Long)holder.getKeys().get("id");
        }

        return null;
    }
}
