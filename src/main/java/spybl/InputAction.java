package spybl;

/**
 * Created by flavia.gheorghe on 02/05/15.
 */
public class InputAction {

    private String type;

    private String id;

    private String value;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
