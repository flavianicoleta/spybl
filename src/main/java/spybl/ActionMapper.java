package spybl;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.jdbc.core.RowMapper;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by flavia.gheorghe on 08/07/15.
 */
public class ActionMapper implements RowMapper {

    @Override
    public Object mapRow(ResultSet resultSet, int i) throws SQLException {
        Action action = new Action();

        action.setId(resultSet.getLong("id"));
        action.setActionType(resultSet.getString("action_type"));
        action.setRegistrationDate(resultSet.getLong("registered_at"));
        action.setSessionId(resultSet.getLong("session_id"));

        try {
            Page page = new ObjectMapper().readValue(resultSet.getString("page"), new TypeReference<Page>(){});
            action.setPage(page);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ScrollAction scrollAction;
        InputAction inputAction;
        LinkAction linkAction;
        SelectTextAction selectTextAction;

        if(action.getActionType().equals("scroll")) {
            try {
                scrollAction = new ObjectMapper().readValue(resultSet.getString("details"), new TypeReference<ScrollAction>(){});
                action.setDetails(scrollAction);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if(action.getActionType().equals("input")) {
            try {
                inputAction = new ObjectMapper().readValue(resultSet.getString("details"), new TypeReference<InputAction>(){});
                action.setDetails(inputAction);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if(action.getActionType().equals("link")) {
            try {
                linkAction = new ObjectMapper().readValue(resultSet.getString("details"), new TypeReference<LinkAction>(){});
                action.setDetails(linkAction);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if(action.getActionType().equals("selectText")) {
            try {
                selectTextAction = new ObjectMapper().readValue(resultSet.getString("details"), new TypeReference<SelectTextAction>(){});
                action.setDetails(selectTextAction);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return action;
    }
}
