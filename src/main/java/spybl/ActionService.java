package spybl;

/**
 * Created by flavia.gheorghe on 08/07/15.
 */
public interface ActionService {
    Action store(String actionType, Object details, long registeredAt, Page page, String sessionUUID);
}
