package spybl;

/**
 * Created by flavia.gheorghe on 26/02/15.
 */
public class ScrollAction {

    private double xPosition;

    private double yPosition;

    public double getxPosition() {
        return xPosition;
    }

    public void setxPosition(double xPosition) {
        this.xPosition = xPosition;
    }

    public double getyPosition() {
        return yPosition;
    }

    public void setyPosition(double yPosition) {
        this.yPosition = yPosition;
    }
}
