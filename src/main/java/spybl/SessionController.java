package spybl;

import com.thesis.spybl.services.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by flavia.gheorghe on 26/02/15.
 */
@Controller
public class SessionController {

    @Autowired
    private SessionService sessionService;


    @RequestMapping(value = "/session/store", produces = "application/json", method = RequestMethod.GET)
    public @ResponseBody Session createNewSession(@RequestParam String uuid,
                                               @RequestParam String url, @RequestParam String title,
                                               @RequestParam String ip, @RequestParam String location) {
        Page page = new Page();
        page.setUrl(url);
        page.setTitle(title);
        return sessionService.store(uuid, page, ip, location, System.currentTimeMillis());
    }

    @RequestMapping(value = "/session/close", produces = "application/json", method = RequestMethod.GET)
    public @ResponseBody Long closeSession(@RequestParam String uuid){
        return sessionService.update(uuid, System.currentTimeMillis());
    }

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public @ResponseBody String test() {
        return "stuff are happening";
    }

}
