package spybl;

/**
 * Created by flavia.gheorghe on 26/02/15.
 */
public interface SessionDAO {
    Session store(String uuid, Object page, String ip, String location, long starDate);
    Long update(String uuid, long endDate);
}
