package spybl;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.jdbc.core.RowMapper;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by flavia.gheorghe on 26/02/15.
 */
public class SessionMapper implements RowMapper {

    @Override
    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
        Session session = new Session();

        session.setId(resultSet.getLong("id"));
        session.setUuid(resultSet.getString("uuid"));
        session.setIp(resultSet.getString("ip"));
        session.setLocation(resultSet.getString("location"));
        session.setStartDate(resultSet.getLong("start_date"));
        session.setEndDate(resultSet.getLong("end_date"));

        try {
            Page page = new ObjectMapper().readValue(resultSet.getString("page"), new TypeReference<Page>(){});
            session.setPage(page);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return session;
    }
}
