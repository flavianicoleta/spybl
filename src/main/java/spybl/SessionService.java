package spybl;

/**
 * Created by flavia.gheorghe on 26/02/15.
 */
public interface SessionService {
    Session store(String uuid, Object page, String ip, String location, long startDate);
    Long update(String uuid, long endDate);
}
