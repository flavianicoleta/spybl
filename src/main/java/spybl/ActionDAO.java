package spybl;

/**
 * Created by flavia.gheorghe on 08/07/15.
 */
public interface ActionDAO {
    Action store(String actionType, Object details, long registration_date, Page page, String sessionUUID);
}
