package com.thesis.spybl.services;

import com.thesis.spybl.dao.ActionDAO;
import com.thesis.spybl.model.Action;
import com.thesis.spybl.model.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by flavia.gheorghe on 08/07/15.
 */
@Service
@Transactional
public class ActionServiceImpl implements ActionService {

    @Autowired
    private ActionDAO actionDAO;

    @Override
    public Action store(String actionType, Object details, long registeredAt, Page page, String sessionUUID) {
        return actionDAO.store(actionType,details, registeredAt, page, sessionUUID);
    }
}
