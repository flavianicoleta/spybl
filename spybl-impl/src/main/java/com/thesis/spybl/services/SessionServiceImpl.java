package com.thesis.spybl.services;

import com.thesis.spybl.dao.SessionDAO;
import com.thesis.spybl.model.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by flavia.gheorghe on 26/02/15.
 */
@Service
@Transactional
public class SessionServiceImpl implements SessionService {

    @Autowired
    private SessionDAO sessionDAO;

    @Override
    public Session store(String uuid, Object page, String ip, String location, long startDate) {
        return sessionDAO.store(uuid, page, ip, location, startDate);
    }

    @Override
    public Long update(String uuid, long endDate) {
        return sessionDAO.update(uuid, endDate);
    }

}
