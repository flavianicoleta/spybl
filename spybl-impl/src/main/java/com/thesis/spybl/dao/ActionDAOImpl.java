package com.thesis.spybl.dao;

import com.thesis.spybl.model.Action;
import com.thesis.spybl.model.Page;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.io.IOException;

/**
 * Created by flavia.gheorghe on 08/07/15.
 */
@Repository
public class ActionDAOImpl implements ActionDAO {

    @Autowired
    @Qualifier(value = "namedJdbcTemplate")
    private NamedParameterJdbcTemplate namedJdbcTemplate;

    @Override
    public Action store(String actionType, Object details, long registration_date, Page page, String sessionUUID) {
        int response = 0;
        String sql = "INSERT INTO actions(action_type, details, registered_at, page, session_id) " +
                "values (:action_type, json(:details), :registration_date, json(:page), (select id from sessions where uuid = :session_uuid))";

        ObjectMapper mapper = new ObjectMapper();
        try {
            String detailsJson = mapper.writeValueAsString(details);
            String pageJson = mapper.writeValueAsString(page);
            MapSqlParameterSource params = new MapSqlParameterSource();

            params.addValue("action_type", actionType);
            params.addValue("details", detailsJson);
            params.addValue("registration_date", registration_date);
            params.addValue("page", pageJson);
            params.addValue("session_uuid", sessionUUID);

            KeyHolder holder = new GeneratedKeyHolder();

            response = namedJdbcTemplate.update(sql, params, holder);

            if(response == 1 && holder.getKeys() != null) {
                long id = (Long)holder.getKeys().get("id");
                long sessionId= (Long)holder.getKeys().get("session_id");
                Action action = new Action(id, actionType, details, page, registration_date, sessionId);
                return action;
            }
        } catch (IOException e) {
            return null;
        }

        return null;
    }
}
